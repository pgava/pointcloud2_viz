import argparse
import sys
import re

def exactly_match_regex(text : str, rgx) -> bool:
    match_obj = re.match(rgx,  text)
    # print(re.findall(rgx, text))
    if(match_obj is not None):
        print(match_obj.group())
        return match_obj.group() == text
    else:
        return False

def match_rosbag_name(rosbag_name : str):
    rgx = re.compile(r'(?:/[^/]+)+?/\w+\.bag')

    match = exactly_match_regex(rosbag_name, rgx)

    if(not match):
        sys.exit("The rosbag file " + rosbag_name + \
            " does not conform with the pattern  " \
            + "-- (?:/[^/]+)+?/\w+\.bag -- . " \
            + "Found patterns were " \
            + ''.join(map(str,re.findall(rgx, rosbag_name))) + " .")

    return match

def match_topic_name(topic_name : str):
    rgx = re.compile(r'(/\w+)+')

    match = exactly_match_regex(topic_name, rgx)

    if(not match):
        sys.exit("The topic name " + topic_name + \
            " does not conform with the pattern  " \
            + "-- (/[a-zA-Z_]+)+ -- . " \
            + "Found patterns were " \
            + ''.join(map(str,re.findall(rgx, topic_name))) + " .")


    return match

def parse_cli_args():
    parser = argparse.ArgumentParser(prog="pointcloud2_viz",\
        description='Extracts PointCloud2 msg from a rosbag and visualize it.')

    parser.add_argument('-b', '--bag-name', dest='bag_name', \
                        type=str, action='store', \
                        required=True, help='Rosbag file name.')

    parser.add_argument('-p', '--pointcloud-topics', nargs='+', dest='pc_topic', \
                        type=str, action='store', \
                        required=True, \
                        help='Topics with PointCloud2. ' \
                             + 'Each PC must have a correspondent frame linked to the world frame.')
    
    parser.add_argument('-t', '--tf-topics', nargs='+', dest='tf_topic', \
                        type=str, action='store', \
                        default=['/tf', '/tf_static'], \
                        help='Topics with tf type msgs.')

    parser.add_argument('-i', '--info', dest='info', action='store', \
                        default=0, nargs=1, \
                        help='Dimension of interest to coloring the point cloud, 1-indexed.'  \
                        + ' Zero means only homogenous color.')

    parser.add_argument('-w', '--world-frame', nargs=1, dest='world_frame', \
                        type=str, action='store', \
                        required=True, \
                        default="site_origin", \
                        help='Common frame to merge pointclouds.')

    parser.add_argument('-v', '--is-live-view', dest='live_view', \
                        type=bool, action='store', nargs=1, \
                        default=False, \
                        help='Enable real time visualization.')

    parser.add_argument('-s', '--save-to-pcd', dest='save_to_pcd', \
                        type=bool, action='store', nargs=1, \
                        default=True, \
                        help='Enable saving the resultand pointclouds to .pcd ' \
                            + 'files in /path/to/bag.bag_pcds/*.pcd.')


    return parser.parse_args()