import rosbag
import rospy
import tf2_py
from sensor_msgs import point_cloud2
import open3d as o3d
import os
import input_handler as ih
from any_obstacle_segmenter.create_map_from_bag import pointcloud2_to_open3d
from any_obstacle_segmenter.create_map_from_bag import color_point_cloud_by_z_threshold
from any_obstacle_segmenter.create_map_from_bag import lookup_transform
from any_obstacle_segmenter.create_map_from_bag import tf2_to_matrix
from any_obstacle_segmenter.create_map_from_bag import remove_points_by_range
from any_obstacle_segmenter.create_map_from_bag import remove_points_by_z
from any_obstacle_segmenter.create_map_from_bag import remove_points_by_bounding_box

from pointcloud_utils import pointcloud2_to_open3d_data_seg
from any_obstacle_segmenter.grid_2d import Grid2D
from any_obstacle_segmenter.grid_2d import Cell
from grid_cell_extended import ext_cell
import cv2
import numpy as np

import sys

class rosbag_topics_holder:

    def __init__(self, rosbag_name : str, \
                 topic_list) -> None:

        ih.match_rosbag_name(rosbag_name)

        for topic_name in topic_list:
            ih.match_topic_name(topic_name)

        self.__rosbag_file_name = rosbag_name
        self.__topic_list = topic_list

        self.__rosbag = rosbag.Bag(self.__rosbag_file_name)

        print(topic_list)

        #check for existance of all topics in the bag
        for topic_name in topic_list:
            if(not topic_name in self.__rosbag.get_type_and_topic_info()[1].keys()):
                sys.exit("Topic " + topic_name + " does not exist in ros bag " \
                        + self.__rosbag_file_name)

    def __del__(self):
        self.__rosbag.close()

    def get_msgs(self):
        return self.__rosbag.read_messages(topics=self.__topic_list)

    def get_bag(self):
        return self.__rosbag

    def close_bag(self):
        self.__rosbag.close()

class pointcloud_processer:

    def __init__(self, rosbag_name : str, \
                 pc_topic_name_list, tf_topic_name_list, info : int = 0) -> None:
        
        self.__topics_holder = rosbag_topics_holder(rosbag_name, \
                                             topic_list=(pc_topic_name_list+tf_topic_name_list))

        self.__rosbag_file_name = rosbag_name
        self.__pc_topic_list = pc_topic_name_list
        self.__tf_topic_list = tf_topic_name_list
        self.__topic_list = self.__pc_topic_list + self.__tf_topic_list

        bag_duration = self.__topics_holder.get_bag().get_end_time() - \
                            self.__topics_holder.get_bag().get_start_time()
        self.__tf_buffer =  tf2_py.BufferCore(rospy.Duration(1000.0))   

    def process(self):
        # map_cloud = self.__heigth_seg()
        map_cloud = self.__doppler_seg()
        # self.__rcs_seg()
        # self.__confidence_seg()

        output_path = os.path.join('', "map_cloud.pcd")
        print(f"Saving point cloud to: {output_path}")
        o3d.io.write_point_cloud(output_path, map_cloud)

        points = np.asarray(map_cloud.points)

        x_min = min(points[:,0])
        x_max = max(points[:,0])
        y_min = min(points[:,1])
        y_max = max(points[:,1])

        resolution = 0.5

        x_size = int((x_max - x_min)/resolution)+1
        y_size = int((y_max - y_min)/resolution)+1

        print(x_size)
        print(y_size)

        # exit()

        grid_origin = (min(points[:,0]), min(points[:,1]))

         # create the grid
        grid = Grid2D(resolution, (x_size, y_size), grid_origin)

        data_for_grid = np.zeros((len(map_cloud.points), 4))

        data_for_grid[:, 0:3] = np.asarray(map_cloud.points)

        normals = np.asarray(map_cloud.normals)

        data_for_grid[:, 3] = (abs(normals[:,0]) - min(abs(normals[:,0])))/(max(abs(normals[:,0])) - min(abs(normals[:,0])))

        print(min(abs(normals[:,0])))
        print(max(abs(normals[:,0])))

        exit()

        # data_for_grid[:, 3] = (normals[:,0] - min(normals[:,0]))/(max(normals[:,0]) - min(normals[:,0]))

        # iterate over the points in the point cloud
        for data in data_for_grid:
            grid_index = grid.to_grid(data[:2])
            print(data)
            print(grid_index)
            if not grid.in_bounds(grid_index):
                continue

            # update elevation
            if grid[grid_index].elevation < (255 - data[3]*255):
                grid[grid_index] = Cell(elevation=(255 - data[3]*255))

        # create an OpenCV image to show the grid elevation
        img = grid.draw(lambda cell: cell.elevation)

        resized_img = cv2.resize(
            img, None, fx=10, fy=10, interpolation=cv2.INTER_LINEAR
        )

        cv2.imwrite("img_inv_abs.png", resized_img)

    def __heigth_seg(self):
        first_run = True

        map_cloud = o3d.geometry.PointCloud()

        for topic, msg, t in self.__topics_holder.get_msgs():

            if(topic in self.__topic_list):

                # if it's a transform, fill the buffer
                if topic in self.__tf_topic_list:
                    for msg_tf in msg.transforms:
                        if topic == "/tf_static":
                            self.__tf_buffer.set_transform_static(msg_tf, "rosbag")
                        else:
                            self.__tf_buffer.set_transform(msg_tf, "rosbag")

                # if it's a point cloud, process it
                elif topic in self.__pc_topic_list:
                    # get the transform from pc frame to base link
                    pc2bl = lookup_transform(
                        self.__tf_buffer,
                        "base_link",
                        msg.header.frame_id,
                        msg.header.stamp,
                    )
                    if pc2bl is None:
                        continue

                    # get the transform from base_link to site origin
                    bl2so = lookup_transform(
                        self.__tf_buffer, "site_origin", "base_link", msg.header.stamp
                    )
                    if bl2so is None:
                        continue

                    # convert point cloud to open3d format
                    o3dpc = pointcloud2_to_open3d(msg)

                    o3dpc, _ = o3dpc.remove_radius_outlier(nb_points=3, radius=1)

                    # o3dpc = remove_points_by_range(o3dpc, 40)

                    pc_bl = o3dpc.transform(tf2_to_matrix(pc2bl))

                    pc_bl = remove_points_by_bounding_box(
                        pc_bl, [[-2, -4, -2], [5, 4, 10]]
                    )
                    
                    pc_so = pc_bl.transform(tf2_to_matrix(bl2so))

                    map_cloud += pc_so
                    map_cloud = map_cloud.voxel_down_sample(0.25)

        # remove points below the ground
        map_cloud = remove_points_by_z(map_cloud, -0.1)
        map_cloud, _ = map_cloud.remove_radius_outlier(nb_points=3, radius=0.3)
        map_cloud = color_point_cloud_by_z_threshold(map_cloud, 0.5)

        return map_cloud

    def __doppler_seg(self):
        first_run = True

        map_cloud = o3d.geometry.PointCloud()

        last_stamp = rospy.Time(0)

        for topic, msg, t in self.__topics_holder.get_msgs():
            if(topic in self.__topic_list):

                # if it's a transform, fill the buffer
                if topic in self.__tf_topic_list:
                    for msg_tf in msg.transforms:
                        if topic == "/tf_static":
                            self.__tf_buffer.set_transform_static(msg_tf, "rosbag")
                        else:
                            self.__tf_buffer.set_transform(msg_tf, "rosbag")

                # if it's a point cloud, process it
                elif topic in self.__pc_topic_list:
                    last_stamp = msg.header.stamp 
                    pc2bl = lookup_transform(
                        self.__tf_buffer,
                        "base_link",
                        msg.header.frame_id,
                        msg.header.stamp,
                    )
                    if pc2bl is None:
                        continue

                    # get the transform from base_link to site origin
                    bl2so = lookup_transform(
                        self.__tf_buffer, "site_origin", "base_link", msg.header.stamp
                    )
                    if bl2so is None:
                        continue

                    # convert point cloud to open3d format
                    o3dpc = pointcloud2_to_open3d_data_seg(msg, 3, 0.1)
                
                    o3dpc, _ = o3dpc.remove_radius_outlier(nb_points=3, radius=1)

                    pc_bl = o3dpc.transform(tf2_to_matrix(pc2bl))

                    pc_bl = remove_points_by_bounding_box(
                        pc_bl, [[-2, -4, -2], [5, 4, 10]]
                    )

                    pc_so = pc_bl.transform(tf2_to_matrix(bl2so))

                    map_cloud += pc_so

                    map_cloud = map_cloud.voxel_down_sample(0.25)

        # remove points below the ground
        map_cloud = remove_points_by_z(map_cloud, -0.1)
        map_cloud, _ = map_cloud.remove_radius_outlier(nb_points=3, radius=0.3)

        return map_cloud

    def __rcs_seg(self):
        pass

    def __confidence_seg(self):
        pass
    
    def __pc_to_pcd_file_output(self):
        pass

    def __pc_to_grid_img_output(self):
        pass

#TODO receive as many point cloud topics as needed and convert them
#TODO receive tf transformations


if __name__ == "__main__":
    args = ih.parse_cli_args()

    pcp = pointcloud_processer(args.bag_name, \
                                args.pc_topic, \
                                args.tf_topic, \
                                args.info)

    pcp.process()
