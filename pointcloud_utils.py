import open3d as o3d
import ros_numpy
from sensor_msgs.msg import PointCloud2
import pandas as pd
import matplotlib.colors as colors
import numpy as np

def pointcloud2_to_open3d_data_seg_interval(
    msg: PointCloud2, data_dimension: int, seg_interval: "list(int)"
) -> o3d.geometry.PointCloud:
    """
    Converts a ROS PointCloud2 message to an Open3D PointCloud and
    color the points based on the intervals of the i-th info of each
    point.

    Args:
        msg (ros_numpy.point_cloud2.PointCloud2): The ROS PointCloud2 message.
        data_dimension (int): The n-th dimension of a point for segmentation. Zero indexed.
        seg_interval (list(int)): 

    Returns:
        open3d.geometry.PointCloud: The Open3D PointCloud.
    """
    pcd = o3d.geometry.PointCloud()
    array_points = ros_numpy.point_cloud2.pointcloud2_to_xyz_array(msg)
    pcd.points = o3d.utility.Vector3dVector(array_points)

    # for point in pcd.points:
    #     print(point)

    rec_array = ros_numpy.point_cloud2.pointcloud2_to_array(msg)
    array = pd.DataFrame(rec_array).to_numpy()

    data_vec = array[:, data_dimension]

    # print(len(data_vec))

    colors_arr = np.zeros((len(data_vec), 3))

    color_vec = colors.BoundaryNorm(boundaries=seg_interval, ncolors=len(seg_interval)-1)

    # print(color_vec(seg_interval))

    for i in range(len(seg_interval)-1):
        cond1 = abs(data_vec) >= seg_interval[i]
        cond2 = abs(data_vec) < seg_interval[i+1]
        cond3 = cond1 & cond2
        # print(cond3)
        if(i == 0):
            colors_arr[abs(data_vec) >= seg_interval[i]] = [0, color_vec(seg_interval)[i], 0]
        else: colors_arr[abs(data_vec) >= seg_interval[i]] = [color_vec(seg_interval)[i], 0, 0]



    # for  item in colors_arr:
    #     if(item[1] > 0):
    #         print(item)

    # exit()

    pcd.colors = o3d.utility.Vector3dVector(colors_arr)

    # pcd.points = o3d.utility.Vector3dVector(array)
    return pcd

def pointcloud2_to_open3d_data_seg(
    msg: PointCloud2, data_dimension: int, threshold : float
) -> o3d.geometry.PointCloud:
    """
    Converts a ROS PointCloud2 message to an Open3D PointCloud and
    color the points based on the intervals of the i-th info of each
    point.

    Args:
        msg (ros_numpy.point_cloud2.PointCloud2): The ROS PointCloud2 message.
        data_dimension (int): The n-th dimension of a point for segmentation. Zero indexed.
        seg_interval (list(int)): 

    Returns:
        open3d.geometry.PointCloud: The Open3D PointCloud.
    """
    pcd = o3d.geometry.PointCloud()
    array_points = ros_numpy.point_cloud2.pointcloud2_to_xyz_array(msg)
    pcd.points = o3d.utility.Vector3dVector(array_points)

    # for point in pcd.points:
    #     print(point)

    rec_array = ros_numpy.point_cloud2.pointcloud2_to_array(msg)
    array = pd.DataFrame(rec_array).to_numpy()

    data_vec = array[:, data_dimension]

    data = np.zeros((len(rec_array), 3))

    data[:,0] = data_vec

    pcd.normals = o3d.utility.Vector3dVector(data)

    # print(np.asarray(pcd.normals))

    # exit()

    colors_arr = np.zeros((len(data_vec), 3))

    # Assign green color to points with z coordinate below threshold
    colors_arr[abs(data_vec) < threshold] = [0, 1, 0]

    # Assign red color to points with z coordinate above threshold
    colors_arr[abs(data_vec) >= threshold] = [1, 0, 0]

    # Assign the colors to the point cloud
    pcd.colors = o3d.utility.Vector3dVector(colors_arr)

    return pcd